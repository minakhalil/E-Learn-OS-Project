import { Component, OnInit,AfterViewChecked, ElementRef, ViewChild,Input } from '@angular/core';
import {RoomService} from '../room.service';


@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit, AfterViewChecked {

  @Input() roomID:any;
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  
 
   me:any;
   messages:any;
    
  
  constructor(private roomService: RoomService) { 
    
  }
  
  ngOnInit() {
    
      this.getchathistory();
      this.scrollToBottom();
     // console.log(this.users.get(3));
      
  
  }

  ngAfterViewInit(): void
   {
     this.roomService.recieveMessage().subscribe((msg)=>this.messages.push(msg))
   }
  ngAfterViewChecked() {        
    this.scrollToBottom();        
} 
scrollToBottom(): void {
  try {
      this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
  } catch(err) { }                 
}
isEmpty(input: string): boolean{
  return (input.replace(/\s/g, "").length > 0 ? false : true);
}
stringreplace(s,index, replacement) {
  return s.substr(0, index) + replacement+ s.substr(index + replacement.length);
}
sendmessage(mes: string){
      if(!this.isEmpty(mes)){
        let ln=mes.length;
        if(mes.substring(ln-1,ln)=="\n"){
          mes=mes.substring(0,ln-1);
        }
        
        //this.messages.push({userid:this.me,message:mes});
        
        this.roomService.sendmessage(mes);

      (<HTMLInputElement>document.getElementById("input")).value="";
    }
  } 

getchathistory(){
  this.roomService.getchathistory(this.roomID).subscribe(
    data => {
      if(!data.error)
      {
        console.log("true "+data.success);
        this.messages=data.success.data;
        this.me=data.success.userid;
      }else{
        console.log("false "+data.error);
      }
    }
  );
}



}
