import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoomRoutingModule } from './room-routing.module';
import { MainRoomComponent } from './main-room/main-room.component';
import { WhiteBoardComponent } from './white-board/white-board.component';
import { NotesComponent } from './notes/notes.component';

import {CanvasWhiteboardModule } from 'ng2-canvas-whiteboard';
import { ChatComponent } from './chat/chat.component';

import {RoomService} from './room.service';

import { HTTP_INTERCEPTORS,HttpClientModule } from '@angular/common/http';
import { AuthInterceptor } from '../auth/auth.interceptor';
import { NgxSlideshowModule } from 'ngx-slideshow';

@NgModule({
  imports: [
    CommonModule,
    RoomRoutingModule,
    CanvasWhiteboardModule,
    HttpClientModule,
    NgxSlideshowModule
  ],
  declarations: [MainRoomComponent, WhiteBoardComponent, 
    NotesComponent,ChatComponent
  ],
  providers:[
    RoomService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
    
  ]
})
export class RoomModule { }
