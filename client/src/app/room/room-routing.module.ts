import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import { MainRoomComponent } from './main-room/main-room.component';
import { AuthGuardService } from '../auth/auth.guard';

const routes: Routes = [
    {
        path: 'room/:id',
        component: MainRoomComponent,
        canActivate:[AuthGuardService]
    },
];

export const RoomRoutingModule: ModuleWithProviders = RouterModule.forChild(routes);
