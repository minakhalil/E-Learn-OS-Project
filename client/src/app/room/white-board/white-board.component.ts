import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy, ElementRef, Input , OnChanges, SimpleChanges, SimpleChange } from '@angular/core';
import 'rxjs/add/observable/fromEvent';
import { Observable } from 'rxjs/Observable';
import { RoomService } from '../room.service';
import { objectCanvas } from './objectCanvas';
import { Subscription } from 'rxjs/Subscription';


@Component({
  selector: 'app-white-board',
  templateUrl: './white-board.component.html',
  styleUrls: ['./white-board.component.css'],
  host: {
    '(window:resize)': 'onResize($event)'
  }
})
export class WhiteBoardComponent implements AfterViewInit, OnDestroy ,  OnChanges {

  @ViewChild('canvasEl') canvasEl: ElementRef;
  @ViewChild('writeBtn') writeButtonEl: ElementRef;
  @Input() roomID: any;
  @Input() userID: number=-1;
  @Input() ctrl: any;
  @Input() state:any;

  writeMode: boolean;
  drawMode: boolean;
  mousedown: boolean = false;
  color: string;
  lastX: number;
  lastY: number;
  currX: any;
  currY: any;
  previousX:any;
  previousY:any;
  globalCompositeOperation: any;
  link: any;
  writePositionX: number;
  writePositionY: number;
  xCoordWriting: number;
  yCoordWriting: number;
  eraseMode: boolean;
  firsttime: boolean = true;
  lastKeyShift: boolean = false;
  image: HTMLImageElement;
  private context: CanvasRenderingContext2D;
  canvasX_starting: number;
  canvasY_starting: number;
  canvasX_ending: number;
  canvasY_ending: number;
  saveBtnTop: number;
  ButtonLeft: number;
  drawButtonTop: number;
  eraseButtonTop: number;
  writeButtonTop: number;
  colorButtonTop: number;
  objCanvas: object;
  objCanvasRec: objectCanvas;
  notSendFlag: boolean;
  recievedOldMessage: boolean;
  subscriptionKeyUp: Subscription;
  subscriptionMouseDown: Subscription;
  subscriptionMouseUp: Subscription;
  subscriptionMouseMove: Subscription;
  subscriptionGetBoard: Subscription;
  subscriptionGetNote: Subscription;
  subscriptionListenBoard: Subscription;
  subscriptionListenNote: Subscription;
  canvasRef :any;
  lastChar :any;
  identifier:any=null;






  constructor(private roomService: RoomService) {
    this.writeMode = false;
    this.drawMode = false;
    this.eraseMode = false;
    this.saveBtnTop = this.canvasX_starting + 120;
    this.ButtonLeft = this.canvasX_starting - 25;
    this.drawButtonTop = this.canvasX_starting - 10;
    this.eraseButtonTop = this.canvasX_starting + 80;
    this.colorButtonTop = this.canvasX_starting + 40;
    this.writeButtonTop = this.canvasY_starting;
    this.objCanvas = {};
    (this.objCanvas as any).clearBoard = false;
    this.color = "#000000";
    
    //this.subscripeToInputEvent();
  }

  ngOnInit(){
    
    this.identifier=this.userID.toString()+this.ctrl.toString();
  
  }
  ngOnChanges(changes: SimpleChanges) {
    const myState: SimpleChange = changes.state;

    
    
    if(myState.isFirstChange())
    {

      

      if(this.ctrl==true)
      {

        this.subscripeToInputEvent(window); //teacher opening wb main
       
        if(this.userID==-1)
        {
          this.listenToBoard();
          this.getBoardHistory();
        }
        else
        {
          this.listenToNote();
      
          this.getNoteHistory();
        }
       
        
        this.writeMode = false;
        this.drawMode = false;
        this.eraseMode = false;
        this.objCanvas = {};
        (this.objCanvas as any).clearBoard = false;
        this.color = "#000000";
        
      }
      else 
      {
        if(this.userID==-1)
        {
          this.getBoardHistory();
          this.listenToBoard();
        }
        else
        {
          this.listenToNote();
        
          this.getNoteHistory();
        }
        
       
      }
    }
    else if(myState.previousValue==0 && myState.currentValue==1)
    {

      
      var cc = document.getElementById(this.identifier)
      //console.log(this.identifier,"da identifider",this.userID);
      
      if(this.ctrl==true)
      {

        this.subscripeToInputEvent(cc); //teacher opening wb main
        if(this.userID==-1)
        {
          this.listenToBoard();
          this.getBoardHistory();
        }
        else
        {
          this.listenToNote();
      
          this.getNoteHistory();
        }
       
        
        this.writeMode = false;
        this.drawMode = false;
        this.eraseMode = false;
        this.objCanvas = {};
        (this.objCanvas as any).clearBoard = false;
        this.color = "#000000";
        
      }
      else 
      {
        if(this.userID==-1)
        {
          this.getBoardHistory();
          this.listenToBoard();
        }
        else
        {
          this.listenToNote();
        
          this.getNoteHistory();
        }
        
       
      }
      
    }
    else if (myState.previousValue==1 && myState.currentValue==0)
    { 
      
      if(this.ctrl==true)
      {

        this.destroyInputEvent();
        if(this.userID==-1)
        {
          this.subscriptionGetBoard.unsubscribe();
         
          this.subscriptionListenBoard.unsubscribe();
         
        }
        else
        {
          this.subscriptionListenNote.unsubscribe();
          this.subscriptionGetNote.unsubscribe();
        }
       
        
      }
      else 
      {
        if(this.userID==-1)
        {
          this.subscriptionGetBoard.unsubscribe();
         
          this.subscriptionListenBoard.unsubscribe(); 
        }
        else
        {
          this.subscriptionListenNote.unsubscribe();
          this.subscriptionGetNote.unsubscribe();
        }
        
       
      }
      this.writeMode = false;
      this.drawMode = false;
      this.eraseMode = false;
      this.objCanvas = {};
      (this.objCanvas as any).clearBoard = false;
      this.color = "#000000";

    }

   
  }


  subscripeToInputEvent(cc){

    this.updateCanvasValues()
   // console.log(cc.width,this.canvasEl.nativeElement.offsetLeft)
    this.subscriptionMouseMove = Observable.fromEvent(cc, "mousemove")
      .subscribe(res => {
        this.updateCanvasValues();

        if (this.mousedown == true && (res as any).clientX > this.canvasX_starting && (res as any).clientX < this.canvasX_ending && (this.drawMode == true || this.eraseMode == true) && (res as any).clientY + window.pageYOffset > this.canvasY_starting && (res as any).clientY + window.pageYOffset < this.canvasY_ending) {
          //those coordinated are relative to document
          this.currX = (res as any).clientX;
          this.currY = (res as any).clientY + window.pageYOffset;
          if (this.firsttime) {
            this.firsttime = false;
            this.lastX = this.currX;
            this.lastY = this.currY;
            (this.objCanvas as any).endPath = false;
          }


          this.draw();
        }


      }
      );

    this.subscriptionMouseDown = Observable.fromEvent(cc, "mousedown").subscribe(res => {
      this.updateCanvasValues();
      
      if ((res as any).buttons == 1 && (res as any).clientX > this.canvasX_starting && (res as any).clientX < this.canvasX_ending && (this.drawMode == true || this.eraseMode == true) && (res as any).clientY + window.pageYOffset > this.canvasY_starting && (res as any).clientY + window.pageYOffset < this.canvasY_ending)
        this.mousedown = true;

    });

    this.subscriptionMouseUp = Observable.fromEvent(cc, "mouseup").subscribe(res => {
      this.updateCanvasValues();

      if ((res as any).clientX > this.canvasX_starting && (res as any).clientX < this.canvasX_ending && (this.drawMode == true || this.eraseMode == true) && (res as any).clientY + window.pageYOffset > this.canvasY_starting && (res as any).clientY + window.pageYOffset < this.canvasY_ending) {
        this.mousedown = false;
        this.firsttime = true;

      }
      else if (this.writeMode == true && (res as any).clientX > this.canvasX_starting && (res as any).clientX < this.canvasX_ending && (res as any).clientY + window.pageYOffset > this.canvasY_starting && (res as any).clientY + window.pageYOffset < this.canvasY_ending) {
        this.writePositionX = (res as any).clientX;
        this.writePositionY = (res as any).clientY;
        this.xCoordWriting = this.writePositionX - this.canvasX_starting - pageXOffset;
        this.yCoordWriting = this.writePositionY - this.canvasY_starting;
      }

      if ((this.drawMode == true || this.eraseMode == true) && (res as any).clientX > this.canvasX_starting && (res as any).clientX < this.canvasX_ending && (res as any).clientY + window.pageYOffset > this.canvasY_starting && (res as any).clientY + window.pageYOffset < this.canvasY_ending) {
        (this.objCanvas as any).endPath = true;
        //SEND OBJECT to indicated last one in this path
        if (this.userID == -1) {
          this.roomService.send(this.objCanvas, true);
        }
        else
          this.roomService.send(this.objCanvas, false);


      }
      //console.log(this.identifier,"byrsm da");

    });




    this.subscriptionKeyUp = Observable.fromEvent(cc, "keyup").subscribe(res => {
      this.updateCanvasValues();
     // console.log(this.writeMode,"keyup",this.userID);
      if (this.writeMode == true) {
        (this.objCanvas as any).draw = false;
        (this.objCanvas as any).color = this.color;
        (this.objCanvas as any).letter = (res as any).key;


        if ((res as any).key == "Tab") {

          this.xCoordWriting += 7;


          if (this.xCoordWriting >= (500)) {
            this.yCoordWriting += 10;
            this.xCoordWriting = 2;
          }

        }
        else if ((res as any).key == "Backspace")
        {
          (this.objCanvas as any).xPos = this.xCoordWriting-7;
          (this.objCanvas as any).yPos = this.yCoordWriting;
          (this.objCanvas as any).letter = this.lastChar;
          (this.objCanvas as any).color="#ffffff";
          this.xCoordWriting -= 7;

          if (this.userID == -1) {
            this.roomService.send(this.objCanvas, true);
          }
          else
            this.roomService.send(this.objCanvas, false);

        }
        else {
            (this.objCanvas as any).xPos = this.xCoordWriting;
            (this.objCanvas as any).yPos = this.yCoordWriting;
            this.lastChar=(this.objCanvas as any).letter;
            this.xCoordWriting += 7;
            if (this.xCoordWriting >= (500)) {
              this.xCoordWriting = 2;
              this.yCoordWriting += 10;
             
            }

            if (this.userID == -1) {
              this.roomService.send(this.objCanvas, true);
            }
            else
              this.roomService.send(this.objCanvas, false);
        }
    

        


      }

    });
  }


  ngAfterViewInit(): void {
   
   
    this.context = (this.canvasEl.nativeElement as HTMLCanvasElement).getContext('2d');
    this.link = document.getElementById("download");
    (this.canvasEl.nativeElement as HTMLCanvasElement).width =500;
    (this.canvasEl.nativeElement as HTMLCanvasElement).height = 300;
    this.canvasRef=document.getElementById("canvasEl");
    setTimeout(() => {
      this.canvasX_starting = this.canvasEl.nativeElement.offsetLeft;
      this.canvasY_starting = this.canvasEl.nativeElement.offsetTop;
      this.canvasX_ending = this.canvasX_starting + 500;
      this.canvasY_ending = this.canvasY_starting + 300;
     
    }, 0)

    
  }

  updateCanvasValues() {
    this.canvasX_starting = this.canvasEl.nativeElement.offsetLeft;
    this.canvasY_starting = this.canvasEl.nativeElement.offsetTop;
    this.canvasX_ending=this.canvasX_starting+500;
    this.canvasY_ending=this.canvasY_starting+300;
  }

  writeBoard() {

    if (this.writeMode == true) {
      this.writeMode = false;
      //console.log("writeModeDisabled",this.identifier);

    }
    else {
      if (this.eraseMode == true) {
        this.context.strokeStyle = this.color;
        this.context.globalCompositeOperation = this.globalCompositeOperation;
       
      }
      this.drawMode = false;
      this.eraseMode = false;
      this.writeMode = true;
     // console.log("writeMode",this.identifier);
     
    }
  }

  drawBoard() {

    if (this.drawMode == true) {
      this.drawMode = false;

    }
    else {
      if (this.eraseMode == true) {
        this.context.strokeStyle = this.color;
        this.context.globalCompositeOperation = this.globalCompositeOperation;
       
      }
      this.writeMode = false;
      this.eraseMode = false;
      this.drawMode = true;
     
    }
  }

  chooseColor(event) {
    this.color = event.target.value;
    this.context.strokeStyle = this.color;
  }


  draw() {


    this.updateCanvasValues();

    /*this.context.beginPath();
    this.context.moveTo((this.lastX-this.canvasX_starting), this.lastY-this.canvasY_starting);
    this.context.lineTo((this.currX-this.canvasX_starting), this.currY-this.canvasY_starting);
    */
   var diffX=this.lastX-this.currX;
   var diffY=this.lastY-this.currY;
   var resolution =true;
    if(Math.abs(diffX)<=4 )
    {
      if(Math.abs(diffY)<=4)
      { 
        resolution=false;
        

      }
    }
    else if (Math.abs(diffY)<=4)
    {
      if(Math.abs(diffX)<=4)
      { 
        resolution=false;
      }
    }

    if(!(this.lastX==this.currX && this.lastY==this.currY))
    {
      if(resolution)
      {
       
      if (this.eraseMode == true) {
        //this.context.lineWidth=8;
        (this.objCanvas as any).lineWidthDraw = 8;
      }
      else {
        //this.context.lineWidth = 3;
        (this.objCanvas as any).lineWidthDraw = 3;
      }
   
      (this.objCanvas as any).color = this.color;
      (this.objCanvas as any).draw = true;
      (this.objCanvas as any).xPos = this.currX - this.canvasX_starting;
      (this.objCanvas as any).yPos = this.currY - this.canvasY_starting;
      (this.objCanvas as any).lastXPos = this.lastX - this.canvasX_starting;
      (this.objCanvas as any).lastYPos = this.lastY - this.canvasY_starting;
      //SEND OBJECT
      //this.roomService.send(this.objCanvas);
      if (this.userID == -1) {
        this.roomService.send(this.objCanvas, true);
      }
      else
        this.roomService.send(this.objCanvas, false);
  
      this.lastX = this.currX;
      this.lastY = this.currY;
      }
      else{
        
      }
      
    }
    else
    {
      
    }

    
    
    
    

  }
  save() {
    var dataURL = (this.canvasEl.nativeElement as HTMLCanvasElement).toDataURL('image/png');
    //link 's id is 'download'
    this.link.href = dataURL;
    this.link.download = "canvasContent";
    this.link.click();

    

  }
  erase() {
    if (this.eraseMode == false) {
    this.globalCompositeOperation = this.context.globalCompositeOperation;
      this.context.globalCompositeOperation = "destination-out";
      this.context.strokeStyle = "rgba(0,0,0,1)";
      this.eraseMode = true;
      this.writeMode = false;
      this.drawMode = false;
      
    }
    else {
      this.eraseMode = false;
      this.context.strokeStyle = this.color;
      this.context.globalCompositeOperation = this.globalCompositeOperation;
    }

  }
  recieveObject(obj) {
    //assume that this function is called when sockets recieved an object 
    //create a new objCanvasRec and fill it with data recieved

    this.objCanvasRec = obj;

    if ((this.objCanvasRec as any).clearBoard == true) {

      this.context.clearRect(0, 0, 500, 300);
    }
    else if (this.objCanvasRec.draw) {

      if (this.objCanvasRec.endPath) {
        //do nothing this is the last message in path
        //momken nkheli socket mtb3tsh message di asln 
      }
      else {
        var lineWidthTemp;
        var globalCompositionTemp;
        var colorTemp;
        lineWidthTemp = this.context.lineWidth;
        globalCompositionTemp = this.context.globalCompositeOperation;
        colorTemp = this.context.strokeStyle;
        //colorTemp=this.color;
        this.context.strokeStyle = this.objCanvasRec.color;
        this.context.lineWidth = this.objCanvasRec.lineWidthDraw;
        if (this.objCanvasRec.lineWidthDraw == 8) {
          this.context.globalCompositeOperation = "destination-out";
          this.context.strokeStyle = "rgba(0,0,0,1)";

        }
        this.context.beginPath();
        this.context.moveTo(this.objCanvasRec.lastXPos, this.objCanvasRec.lastYPos);
        this.context.lineTo(this.objCanvasRec.xPos, this.objCanvasRec.yPos);
        this.context.stroke(); //this one that draw
        this.context.closePath();
        this.context.strokeStyle = colorTemp;
        this.context.lineWidth = lineWidthTemp;
        if (this.objCanvasRec.lineWidthDraw == 8) {
          this.context.globalCompositeOperation = globalCompositionTemp;

        }

      }
    }
    else {


     
      if (this.objCanvasRec.letter != "Tab") {
        this.context.font = "15px monospace";
        this.context.fillStyle = this.objCanvasRec.color;
        this.context.fillText(this.objCanvasRec.letter, this.objCanvasRec.xPos, this.objCanvasRec.yPos);
        this.context.strokeStyle = this.color;
      }


    }



  }


  /*
  event binding is all about. 
  It's one-way data binding, in that it sends information from the view to the component class. 
  This is opposite from property binding, 
  where data is sent from the component class to the view.*/
  onResize(event) {
   
    this.canvasX_starting = this.canvasEl.nativeElement.offsetLeft;
    this.canvasY_starting = this.canvasEl.nativeElement.offsetTop;
    this.canvasX_ending = this.canvasX_starting + 500;
    this.canvasY_ending = this.canvasY_starting + 300;

  }
  getBoardHistory() {
    this.subscriptionGetBoard = this.roomService.getBoardHistory(this.roomID).subscribe(
      data => {
        var data2 = JSON.parse(data.paths);

        if (data2) {

          for (var path in data2) {
            for (var points in data2[path]) {
              var elZeft = data2[path][points];

              this.recieveObject(elZeft);

            }

          }

          //iterate and call recieve
        } else {

        }
        this.recievedOldMessage = true;
      }
    );
  }

  getNoteHistory() {
    this.subscriptionGetNote = this.roomService.getNoteHistory(this.roomID, this.userID).subscribe(
      data => {
        var data2 = JSON.parse(data.paths);
        
        if (data2) {

          for (var path in data2) {
            for (var points in data2[path]) {
              var elZeft = data2[path][points];

              this.recieveObject(elZeft);

            }

          }

          //iterate and call recieve
        } else {

        }
        this.recievedOldMessage = true;
      }
    );
  }

  listenToBoard() {
    this.subscriptionListenBoard = this.roomService.onGetCanvas().subscribe(
      data => {
        this.recieveObject(data);
      }
    );
  }

  listenToNote() {
    this.subscriptionListenNote = this.roomService.onGetNote(this.userID).subscribe(
      data => {
        this.recieveObject(data);
      }
    );
  }

  clear() {

    (this.objCanvas as any).clearBoard = true;
    (this.objCanvas as any).endPath = true;
    if (this.userID == -1) {
      
      (this.objCanvas as any).image = (this.canvasEl.nativeElement as HTMLCanvasElement).toDataURL('image/png')
      this.roomService.send(this.objCanvas, true);
    }
    else
      this.roomService.send(this.objCanvas, false);

    (this.objCanvas as any).clearBoard = false;


  }
  ngOnDestroy() {
   
  }
  destroy(){
      
    
  }
  destroyInputEvent()
  {
    //this.subscriptionKeyUp.unsubscribe();
    this.subscriptionMouseDown.unsubscribe();
    this.subscriptionMouseMove.unsubscribe();
    this.subscriptionMouseUp.unsubscribe();

  }



}