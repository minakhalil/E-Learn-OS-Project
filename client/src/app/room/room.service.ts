import { Injectable } from '@angular/core';
import { Http, HttpModule, Headers, RequestOptions } from '@angular/http';
import { HttpParams, HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';
import {environment} from '../../environments/environment';
import 'rxjs/Rx';
import * as socketIo from 'socket.io-client';
import { objectCanvas } from './white-board/objectCanvas';

const mainApiEndpoint = environment.apiEndPoint+"/api/";


@Injectable()
export class RoomService {
    http: HttpClient;
    roomID:any;
    private socket;
    userID = localStorage.getItem("user_data");
    
    
    public initSocket(roomid): void {
        var user=JSON.parse(localStorage.getItem("user_data_obj"))
        this.roomID = roomid;
        (user as any).roomID = roomid;
        this.socket = socketIo(environment.apiEndPoint);
        this.socket.emit('join-room',user)
    }

    public send(message: object,canvas : boolean): void {
        (message as any).userID = parseInt(this.userID);
        (message as any).roomID = this.roomID;
        if(canvas)
        this.socket.emit('canvas-data', message);
        else
        this.socket.emit('user-data', message);
    }

    public onGetCanvas(): Observable<objectCanvas> {
        return new Observable<objectCanvas>(observer => {
            this.socket.on('send-canvas-to-front', (data: objectCanvas) => observer.next(data));
        });
    }

    public onGetNote(id): Observable<objectCanvas> {
        return new Observable<objectCanvas>(observer => {
            this.socket.on('user-data-'+id, (data: objectCanvas) => observer.next(data));
        });
    }

    public newUserJoined(): Observable<objectCanvas> {
        return new Observable<objectCanvas>(observer => {
            this.socket.on('new-user-joined', (data: objectCanvas) => observer.next(data));
        });
    }

    //we created an observable with the event this.socket on message , 
    //on message will make a call back kol lazmto eno by7ot el data f observable object
    

    public onEvent(event: Event): Observable<any> {
        return new Observable<Event>(observer => {
            this.socket.on(event, () => observer.next());
        });
    }

    constructor(http: HttpClient) {
        this.http = http;

    }

   
    sendmessage(chatmessage) {
        var msg = {};
        (msg as any).userid = parseInt(this.userID);
        (msg as any).roomID = this.roomID;
        (msg as any).message = chatmessage;
        (msg as any).avatar = JSON.parse(localStorage.getItem("user_data_obj")).avatar;
        this.socket.emit('send-message', msg);
    }

    public recieveMessage(): Observable<objectCanvas> {
        return new Observable(observer => {
            this.socket.on('new-message', (data) => observer.next(data));
        });
    }

    getchathistory(roomid) {
        
        return this.http.get(mainApiEndpoint + "room/message/"+roomid)
            .catch(this.handleError);
    }
    getOldBoards(roomid) {
        
        return this.http.get(mainApiEndpoint + "room/"+roomid+"/history")
            .catch(this.handleError);
    }
    requestToJoinRoom(roomid)
    {
        return this.http.get(mainApiEndpoint + "room/"+roomid)
            .catch(this.handleError);
    }
    getBoardHistory(roomid)
    {
        return this.http.get(mainApiEndpoint + "room/"+roomid+"/getcanvas")
            .catch(this.handleError);
    }
    getNoteHistory(roomid,userID)
    {
        return this.http.post(mainApiEndpoint + "room/"+roomid+"/notes",{ user: userID  })
            .catch(this.handleError);
    }


    handleError(error) {
        return Observable.throw(error);
    }

}