import { Component, OnInit } from '@angular/core';
import { WhiteBoardComponent } from '../white-board/white-board.component';
import { NotesComponent } from '../notes/notes.component';
import { Router, ActivatedRoute } from '@angular/router';
import { RoomService } from '../room.service';


@Component({
  selector: 'app-main-room',
  templateUrl: './main-room.component.html',
  styleUrls: ['./main-room.component.css']
})
export class MainRoomComponent implements OnInit {
  id: number;
  users: any;
  roomInfo: any;
  socketInitialized: boolean = false;
  selected: any = "main";
  currentUser = JSON.parse(localStorage.getItem("user_data_obj"));
  oldBoards:any=[];

  


  constructor(private roomService: RoomService, private router: Router, private route: ActivatedRoute) {



  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number         
      console.log(this.id);
    });
    //this.roomService.requestToJoinRoom(this.id);
    this.joinRoom();

  }
  joinRoom() {

    this.roomService.requestToJoinRoom(this.id).subscribe(
      data => {

        if (data.msg) {
          this.users = data.users;
          this.roomInfo = data.room;
          if(this.users)
          this.users.sort(function (a, b) { return a['type'] - b['type'] });
          this.roomService.initSocket(this.id);
          this.socketInitialized = true;
          this.getNewUserJoined();
        } else {
          console.log("false " + data.error);
        }
      }
    );
  }

  getNewUserJoined() {
    this.roomService.newUserJoined().subscribe(user => {
      var result = this.users.filter(function (userInList) {
        return userInList.id == (user as any).id;
      });
      
      if(result.length ==0)
      this.users.push(user)

      if(this.users)
          this.users.sort(function (a, b) { return a['type'] - b['type'] });
    })
  }

  setSelectedTab(tab) {
    this.selected = tab;
  }

  goToMyProfile() {
    this.router.navigate(["auth/profile"])
  }


  getOldBoards(){
    this.roomService.getOldBoards(this.id).subscribe(
      data => {
        this.oldBoards = data.images;
        console.log(this.oldBoards)
      });
  }


}


