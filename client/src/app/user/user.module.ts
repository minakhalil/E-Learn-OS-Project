import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserRoutingModule } from './user-routing.module';

import { UserTestComponent } from 'app/user/testComponent/user.component';


@NgModule({
  imports: [
    CommonModule,
    UserRoutingModule
  ],
  declarations: [UserTestComponent]
})
export class UserModule { }
