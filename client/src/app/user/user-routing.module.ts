import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';

import { UserTestComponent } from './testComponent/user.component';


const routes: Routes = [
    {
        path: 'u',
        component: UserTestComponent,
    },
];

export const UserRoutingModule : ModuleWithProviders = RouterModule.forChild(routes);
