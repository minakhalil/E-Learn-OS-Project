import { NgModule, ApplicationRef } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import "materialize-css";
import { AppComponent } from './app.component';
import { UserModule } from './user/user.module';
import { RoomModule } from './room/room.module';
import { AuthModule } from './auth/auth.module';
import { AppRoutingModule } from './app.routing';
import { MaterializeModule } from "angular2-materialize";
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './auth/auth.interceptor';
import { NgxSlideshowModule } from 'ngx-slideshow';
//sub modules : https://stackoverflow.com/questions/47353218/multiple-modules-and-routing-in-angular-5?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa

//the main module imports the sub modules (room, user Module)
// and the routing module?

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserModule,
    UserModule,
    RoomModule,
    AuthModule,
    HttpModule,
    MaterializeModule,
    NgxSlideshowModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
