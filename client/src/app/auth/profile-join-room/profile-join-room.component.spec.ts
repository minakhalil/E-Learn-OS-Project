import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileJoinRoomComponent } from './profile-join-room.component';

describe('ProfileJoinRoomComponent', () => {
  let component: ProfileJoinRoomComponent;
  let fixture: ComponentFixture<ProfileJoinRoomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileJoinRoomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileJoinRoomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
