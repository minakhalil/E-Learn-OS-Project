import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile-join-room',
  templateUrl: './profile-join-room.component.html',
  styleUrls: ['./profile-join-room.component.css']
})
export class ProfileJoinRoomComponent implements OnInit {

  @ViewChild('modalClose') modalClose: ElementRef;
  name: any;
  errorMsg: string = null;
  currentUser = JSON.parse(localStorage.getItem("user_data_obj"))
  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
  }

  createRoom() {

    this.authService.createRoom(this.name).subscribe(
      data => {
        if (data.err) {
          this.errorMsg = data.err;
        } else {
          this.modalClose.nativeElement.click();
          this.errorMsg = null;
          setTimeout(() => {
            location.reload();
          },200)

        }


      },
      err => { this.errorMsg = err.error.err; },
      () => { }
    );


  }
}
