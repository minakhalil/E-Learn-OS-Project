import { Component, OnInit } from '@angular/core';
import {
    ReactiveFormsModule,
    FormsModule,
    FormGroup,
    FormControl,
    Validators,
    FormBuilder
} from '@angular/forms';
import {AuthService} from '../auth.service';

import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css','./login.component.util.css' ]
})
export class LoginComponent implements OnInit {
loginForm:FormGroup;
username: FormControl;
password: FormControl;
errorMsg:string=null;
  constructor(private authService: AuthService,private router:Router) {
    this.username = new FormControl('', Validators.required);
    this.password = new FormControl('', [
      Validators.required
    ]);

    this.loginForm = new FormGroup({
      username: this.username,
      password: this.password
    });
  }

  ngOnInit() {
  }

  login(){
    this.authService.login(this.loginForm.value.username,this.loginForm.value.password).subscribe(
      data => {
        localStorage.setItem("user_token",data.token);
        localStorage.setItem("user_data",data.user.id);
        localStorage.setItem("user_data_obj",JSON.stringify(data.user));
        this.errorMsg=null;
        this.router.navigate(["auth/profile"])
      },
      err => { this.errorMsg = err.error.err; },
      () => {}
    );

  }

}
