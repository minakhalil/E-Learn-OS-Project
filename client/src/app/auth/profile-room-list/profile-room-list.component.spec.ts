import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileRoomListComponent } from './profile-room-list.component';

describe('ProfileRoomListComponent', () => {
  let component: ProfileRoomListComponent;
  let fixture: ComponentFixture<ProfileRoomListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileRoomListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileRoomListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
