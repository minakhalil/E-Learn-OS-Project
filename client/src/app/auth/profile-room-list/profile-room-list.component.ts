import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-profile-room-list',
  templateUrl: './profile-room-list.component.html',
  styleUrls: ['./profile-room-list.component.css']
})
export class ProfileRoomListComponent implements OnInit {

  arr = [];
  list = null;
  constructor(private authService: AuthService,private router: Router) { }

  ngOnInit() {
    this.authService.getUserRooms().subscribe(
      data => { 
        this.arr = data.reverse(); },
      err => { console.log(err); },
      () => {}
    );
  }

  //after the ngfor has created the list items
  ngAfterViewInit() {
    this.list = document.getElementById("parentOfListItems");
    this.scrollFunc();
  }

  scrollFunc(){
    for (let i = 0; i < this.list.childNodes.length; i++) {
      if (this.list.childNodes[i].localName == "li"){
        let yPos = this.list.childNodes[i].getBoundingClientRect().top;
        let opacity = window.innerHeight - yPos;
        opacity += 200;
        this.list.childNodes[i].style.opacity = opacity / window.innerHeight;
      }
    }
  }

  navToRoom(id){
    this.router.navigateByUrl('/room/'+id);
  }

}
