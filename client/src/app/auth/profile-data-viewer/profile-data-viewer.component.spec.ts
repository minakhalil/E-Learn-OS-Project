import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileDataViewerComponent } from './profile-data-viewer.component';

describe('ProfileDataViewerComponent', () => {
  let component: ProfileDataViewerComponent;
  let fixture: ComponentFixture<ProfileDataViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileDataViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileDataViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
