import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.service';
import {Router} from '@angular/router';
@Component({
  selector: 'app-profile-data-viewer',
  templateUrl: './profile-data-viewer.component.html',
  styleUrls: ['./profile-data-viewer.component.css']
})
export class ProfileDataViewerComponent implements OnInit {

  userData:any =null;
  constructor(private authService: AuthService,private router:Router) { }

  ngOnInit() {
    this.authService.getUser().subscribe(
      data => { this.userData=data.user;},
      err => { console.log(err) },
      () => {}
    );
  }

  logout(){
    this.authService.logout();
    this.router.navigate(["auth/login"]);
  }

}
