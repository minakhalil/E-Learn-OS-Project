import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthRoutingModule } from './auth-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { ProfileContainerComponent } from './profile-container/profile-container.component';
import { ProfileDataViewerComponent } from './profile-data-viewer/profile-data-viewer.component';
import { ProfileJoinRoomComponent } from './profile-join-room/profile-join-room.component';
import { ProfileRoomListComponent } from './profile-room-list/profile-room-list.component';
import { HttpModule } from '@angular/http';
import {AuthService} from './auth.service';
import { HTTP_INTERCEPTORS,HttpClientModule } from '@angular/common/http';
import { AuthInterceptor } from './auth.interceptor';
import { AuthGuardService } from './auth.guard';
import { UnAuthGuardService } from './unauth.guard';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    
    HttpClientModule,
    AuthRoutingModule
  ],
  declarations: [
    LoginComponent,
    ProfileContainerComponent,
    ProfileDataViewerComponent,
    ProfileJoinRoomComponent,
    ProfileRoomListComponent
  ],
  providers:[
    AuthService,
    AuthGuardService,
    UnAuthGuardService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
    
  ]
})
export class AuthModule { }
