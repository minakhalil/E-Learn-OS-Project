import { Injectable } from '@angular/core';
import { Http, HttpModule, Headers, RequestOptions } from '@angular/http';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import 'rxjs/Rx';

const mainApiEndpoint = environment.apiEndPoint + "/api/";


@Injectable()
export class AuthService {
    http: HttpClient;

    constructor(http: HttpClient) {
        this.http = http;

    }

    login(username, password) {
        return this.http.post(mainApiEndpoint + "user/login", { username, password })

            .catch(this.handleError);
    }

    createRoom(name) {
        return this.http.post(mainApiEndpoint + "room/create", { name })

            .catch(this.handleError);
    }
    getUser() {
        return this.http.get(mainApiEndpoint + "user/me")
            .catch(this.handleError);
    }

    logout() {
        localStorage.removeItem("user_token");
        localStorage.removeItem("user_data");
        localStorage.removeItem("user_data_obj");
    }

    getUserRooms() { // the Authorization token with contains the token for the user (which the back uses to get user id)
        // is put into the request by the auth.interceptor <3
        return this.http.get(mainApiEndpoint + "user/rooms")
            .catch(this.handleError);
    }

    handleError(error) {
        return Observable.throw(error);
    }

}
