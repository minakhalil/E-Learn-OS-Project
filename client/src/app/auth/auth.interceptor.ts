import { Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpErrorResponse
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class AuthInterceptor implements HttpInterceptor {


    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        const token = localStorage.getItem("user_token");
        

        if (token) {
            const cloned = req.clone({
                headers: req.headers.set("Authorization", token)
            });

            return next.handle(cloned).do(
                (err: any) => {
                    if (err instanceof HttpErrorResponse) {

                        if (err.status === 401) {
                            window.location.href ="/auth/login";
                        }
                    }
                }
            );
        }
        else {
            return next.handle(req);
        }
    }
}