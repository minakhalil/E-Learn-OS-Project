
import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from './auth.service';
@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(public auth: AuthService, public router: Router) {}

  isAuthenticated(): boolean{
      if (localStorage.getItem("user_token")) {
        return true;
      }
      else{
        return false;
      }
    }


  canActivate(): boolean {
    if (!this.isAuthenticated()) {
      this.router.navigate(['auth/login']);
      return false;
    }
    return true;
  }
}