import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import { AuthGuardService } from './auth.guard';
import { UnAuthGuardService } from './unauth.guard';
import { LoginComponent } from './login/login.component';
import { ProfileContainerComponent } from './profile-container/profile-container.component';


const routes: Routes = [
    {
        path: 'login',
        component: LoginComponent,
        canActivate:[UnAuthGuardService]

        
    },
    {
        path: 'profile',
        component: ProfileContainerComponent,
        canActivate:[AuthGuardService]
    },
];

export const AuthRoutingModule: ModuleWithProviders = RouterModule.forChild(routes);
