import { RouterModule, Routes } from '@angular/router';
import {ModuleWithProviders} from '@angular/core';

import {AppComponent} from './app.component';
import {UserModule} from './user/user.module';
import {RoomModule} from './room/room.module';
import {AuthModule} from './auth/auth.module';

const routes: Routes = [

    {path: 'room', loadChildren: './room/room.module#RoomModule' },
    {path: 'auth', loadChildren: './auth/auth.module#AuthModule' },
    {path: '', redirectTo: 'auth/profile', pathMatch: 'full'}, 
    
];

export const AppRoutingModule: ModuleWithProviders = RouterModule.forRoot(routes, { useHash: true });
