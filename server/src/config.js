import Sequelize from 'sequelize';
import fs from 'fs'
/*
Notes to myself about importing [Bassem]:
  - importing a file runs it's contents only once.
    ex: a console log in the file will show up only once
  - any change that happen to a parameter/object which was exported
    from a file are visible to all importing files! WOW !
*/

//clear the log file -- el function di btetdrab lw mfish file
//a7san eni a5aliha eni a write space fi file , fa hy overwrite
//fs.truncateSync('./mongodb.log', 0);
fs.writeFileSync('./mongodb.log', '');

const sql = new Sequelize('team17db', 'team17', 'team17P123', {
  host: 'localhost',
  dialect: 'mysql',
  operatorsAliases: false,
  logging: myLogFunc,

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
});

//logging function. append to the log file
function myLogFunc (msg){
  fs.appendFile('./mongodb.log', msg + "\n", function (err) {
    if (err) throw err;
  });
}

async function connectDB() {
  try{
  var m = await sql.authenticate();
  console.log('Connection has been established successfully, check mongodb.log for activity log');

  }catch(e){
  console.log(e);
  }
};

connectDB();

const jwtSecret = "hwa-dah-elbatal-ellyda7a-bl7yah-<3-ya-siiiseeeeeeeeyyyyyyyyy-ya-siiseeeyyyyyyyy";
//to be used in token , the browser will send the token each time it will request something from server 
export{
  sql,
  jwtSecret
};
