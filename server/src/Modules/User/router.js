import express from 'express';
import jwt from 'jsonwebtoken';
import verifyLogin from '../../Middlewares/auth'
const router  = express.Router();

import {
login,
signup,
logout,
profile,
rooms
} from './controller';

router.post("/login",login);
router.post("/signup",signup);
router.get("/logout",verifyLogin,logout);
router.get("/me",verifyLogin,profile);
router.get("/rooms",verifyLogin,rooms);

export default router ;
