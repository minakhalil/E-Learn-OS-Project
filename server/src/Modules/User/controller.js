
import User from '../../Model/UserModel';
import Room from '../../Model/RoomModel';
import UserRoom from '../../Model/UserRooms';
import '../../Model/relations'; //this should have a place to be imported in, and it should be imported at least once!!
//it shouldn't depend on being imported here

import Sequelize from 'sequelize';
// it's func are async
const Op = Sequelize.Op;

import jwt from 'jsonwebtoken';
import { sql,jwtSecret } from '../../config';
import bcrypt from 'bcryptjs';

const login = async (req, res) => {

    try {
        var user = await User.findOne({ where: { username: req.body.username } })

        if (!user) return res.status(404).send({ err: 'no user found' });

        var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
        if (!passwordIsValid) return res.status(401).send({ err: 'wrong password' });

        // [Bassem]: el object el bnedih le function sign da hwa el payload, el byb2a mawgood gwa el token metshafar
        //           wel jwtsecret da string byghayar tari2et el hashing, w hwa 3andna sabet, fa kol mra el token
        //           httla3 hya hya le nafs el user.id
        var token = jwt.sign({ id: user.id }, jwtSecret, {
            expiresIn: 86400
        });


        res.status(200).send({ user, token });


    } catch (e) {
        console.log(e);
        return res.status(500).send({ err: 'Error on the server.' });
    }

}


const signup = (req, res) => {
    res.send("hii from signup");

    User.create({ username: "dd", showname: "dd", password: '12345', createdAt: new Date(), updatedAt: new Date() });

}

const logout = (req, res) => {
    res.send("hii from logout");
}

const profile = async (req, res) => {

    try {
        var user = await User.findOne({ where: { id: req.userId } })
        if (!user) return res.status(404).send({ err: 'no user found' });
        res.status(200).send({ user });

    } catch (e) {
        console.log(e);
        return res.status(500).send({ err: 'Error on the server.' });
    }
}

const rooms = async (req, res) => {
    try {
        sql.query(` SELECT *
                    FROM UserRooms as ur
                    INNER JOIN Rooms as r
	                    ON r.id = ur.room_id
                    INNER JOIN Users as u
	                   ON u.id = ur.user_id
                    WHERE ur.user_id =` + req.userId + ";")
        .then(function(myTableRows) {
          res.send(myTableRows[0]);
        });

    } catch (e) {
        console.log(e);
        return res.status(500).send({ err: 'Error on the server.' });
    }
}



export {
    login,
    signup,
    logout,
    profile,
    rooms
}
