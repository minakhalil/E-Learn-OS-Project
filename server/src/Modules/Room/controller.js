import '../../Model/relations';
import Room from '../../Model/RoomModel';
import User from '../../Model/UserModel';
import Note from '../../Model/NoteModel';
import UserRoom from '../../Model/UserRooms';
import Message from '../../Model/MessageModel'
import Sequelize from 'sequelize';
const Op = Sequelize.Op;
import { sql } from '../../config';

const createRoom = async (req, res) => {

  var result;

  try {
    const room = await Room.findOne({ where: { name: req.body.name } });
    if (!room) {
      result = await Room.create({
        name: req.body.name,
        text: null,
        paths: null,
        createdAt: new Date(),
        updatedAt: new Date(),
        OwnerId: req.userId
      });
      const user = await User.findOne({ where: { id: req.userId } });
      result.addUser([user]);
      res.send({ room: result });
    }
    else {
      return res.status(200).send({ err: 'choose another name' });
    }
  }
  catch (e) {
    console.log(e);
    return res.status(500).send({ err: 'error in server' });
    //to do not sure of number of error 
  }
  /* notes
  .create is asynchronous function but i want to send the id after it's created 
  so i have 2 option use async keyword and use await on the function 
  use .then and what's inside will execute after the execution of the async function 
  it will also take as a paramter what the first function returned 
  */
}



const getCanvas = async (req, res) => {


  const room = await Room.findOne({ where: { id: req.params.id } });
  res.status(200).send({ paths: room.paths });

}
const joinRoom = async (req, res) => {
  var idRoom = req.params.id;
  try {
    var result;
    result = await Room.findOne({ where: { id: idRoom } });

    if (!result) {
      res.status(200).send({ msg: 'wrong room id' });
    }
    const users = await result.getUsers();

    const roomUserRelation = await UserRoom.findOne({ where: { room_id: idRoom, user_id: req.userId } });
    if (!roomUserRelation) {

      const user = await User.findOne({ where: { id: req.userId } });
      result.addUser([user]);


    }
    res.status(200).send({ msg: 'joined room', users: users,room:result });
  }
  catch (e) {
    console.log(e);
    return res.status(500).send({ err: 'error in server' });
  }



}

//////////////////////////////author:bishoy kamal///////////////////////
function isEmpty(input) {
  return (input.replace(/\s/g, "").length > 0 ? false : true);
}

const createMessage = async (msg) => {



  if (msg.message === undefined) {
    return
  } else if (isEmpty(msg.message)) {
    return
  }



  try {
    await Message.create({
      message: msg.message,
      createdAt: new Date(),
      updatedAt: new Date(),
      UserId: msg.userid,
      RoomId: msg.roomID
    });


  } catch (e) {
    console.log(e);


  }

}

const getchat = async (req, res) => {

  //console.log(req.params);
  var result;
  try {
    sql.query('Select U.id as userid, U.showname,U.avatar,M.id as messageid,M.message from `team17db`.Users as U,`team17db`.Messages as M where M.RoomId=:rid and U.id=M.UserId',
      { replacements: { rid: req.params.id }, type: Sequelize.QueryTypes.SELECT }
    ).then(result => {
      let succ = {};
      succ.data = result;
      succ.userid = req.userId;
      res.status(200).send({ success: succ, error: null });
    });
  } catch (e) {
    console.log(e);
    res.status(500).send({ success: null, error: "error in database" });

  }

}

const getUserNotes = async (req, res) => {

  const roomID = req.params.id;
  const userID = req.body.user;


  var note = await Note.findOne({ where: { UserId: userID, RoomId: roomID } });


  if (!note) {
    note = {}
    note.paths = null;
  }
  res.status(200).send({ paths: note.paths });

}

const getRoomHistory = async(req,res)=>{
  const roomID = req.params.id;

  const imgs = await sql.query('Select img from `team17db`.RoomHistory where room_id=:rid ',
      { replacements: { rid: req.params.id }, type: Sequelize.QueryTypes.SELECT }
    );

    res.status(200).send({ images: imgs });
}

////////////////////////////////////////////////////////////////////////
//////////////////////////////author:bishoy kamal//////////////////////

/*async function join(function(req,res){

}){

}*/

/*const control ={
  in case i want to make them all in one object 
  createRoom,
  joinRoom
}
export default control*/
export {
  createRoom,
  joinRoom,
  getCanvas,
  createMessage,
  getchat,
  getUserNotes,
  getRoomHistory
}

