import Room from '../../Model/RoomModel';
import Sequelize from 'sequelize';
import Note from '../../Model/NoteModel';
const Op = Sequelize.Op;
import { sql } from '../../config';
import { createMessage } from './controller';

export default (io) => {

    const rooms = {};
    const notes = {};

    io.on('connection', (socket) => {

        console.log('user connected');

        socket.on('join-room', function (user) {
            socket.join(user.roomID);
            socket.broadcast.to(user.roomID).emit('new-user-joined', user);
            console.log("usr joined room", user.roomID)
        });

        socket.on('send-message', async (message) => {

            io.sockets.to(message.roomID).emit('new-message', message);
            await createMessage(message);
        });

        socket.on('canvas-data', async (data) => {

            const userID = data.userID;
            const roomID = data.roomID;
            var img = "";
             if (data.clearBoard == true) {
                 img = data.image
             }
             delete data['image'];
            io.sockets.to(roomID).emit('send-canvas-to-front', data);
            const room = await Room.findOne({ where: { id: roomID } })
            var path = [];



            if (data.clearBoard == true) {

                await sql.query('UPDATE `team17db`.Rooms SET paths=:pt WHERE id=:rid',
                    { replacements: { pt: null, rid: roomID }, type: Sequelize.QueryTypes.UPDATE })
                //rooms[userID] = [];
                //update eno yshil kol eli f room
                const userIDs = await sql.query('SELECT user_id FROM `team17db`.UserRooms  WHERE room_id=:rid',
                    { replacements: { rid: roomID }, type: Sequelize.QueryTypes.SELECT })
                
                
                for (var i = 0; i < userIDs.length; i++) 
                    rooms[userIDs[i].user_id] = []
                
                sql.query('INSERT INTO  `team17db`.RoomHistory (img,room_id)  VALUES (:img,:rid)',
                    { replacements: { rid: roomID,img:img }, type: Sequelize.QueryTypes.INSERT })

            }
            else if (data.draw == false) {
                try {
                    var roomPath = [];
                    if (room.paths) {
                        roomPath = JSON.parse(room.paths);
                    }
                    roomPath.push([data]);
                    
                    await sql.query('UPDATE `team17db`.Rooms SET paths=:pt WHERE id=:rid',
                        { replacements: { pt: JSON.stringify(roomPath), rid: roomID }, type: Sequelize.QueryTypes.UPDATE })

                } catch (e) {
                    console.log(e);

                }

            }
            else if (data.endPath != true) {
                if (!rooms[data.userID]) {
                    rooms[data.userID] = [];
                }

                delete data['userID'];
                delete data['roomID'];
                

                rooms[userID].push(data);

            }
            else {

                try {


                    if (room.paths)
                        path = JSON.parse(room.paths)

                    path.push(rooms[userID]);


                    await sql.query('UPDATE `team17db`.Rooms SET paths=:pt WHERE id=:rid',
                        { replacements: { pt: JSON.stringify(path), rid: roomID }, type: Sequelize.QueryTypes.UPDATE })

                    rooms[userID] = [];



                } catch (e) {
                    console.log(e);

                }

            }

        });




        socket.on('user-data', async (data) => {

            const userID = data.userID;
            const roomID = data.roomID;
            const note = await Note.findOne({ where: { UserId: userID, RoomId: roomID } });
            var path = [];
            io.sockets.to(roomID).emit('user-data-' + userID, data);
            
            if (data.clearBoard == true) {


                await sql.query('UPDATE `team17db`.Notes SET paths=:pt WHERE UserId=:uid AND RoomId=:rid',
                { replacements: { pt: null, uid: userID, rid: roomID }, type: Sequelize.QueryTypes.UPDATE })

                notes[userID] = [];
                
            }
            else if (data.draw == false) {
               
                try {
                   
                    
                    if (note) {

                        if (note.paths)
                           path = JSON.parse(note.paths)

                    }

                    path.push([data]);


                    if (note) {

                        await sql.query('UPDATE `team17db`.Notes SET paths=:pt WHERE UserId=:uid AND RoomId=:rid',
                            { replacements: { pt: JSON.stringify(path), uid: userID, rid: roomID }, type: Sequelize.QueryTypes.UPDATE })

                    } else {

                        var newNote = await Note.create({
                            text: null,
                            paths: JSON.stringify(path),
                            createdAt: new Date(),
                            updatedAt: new Date(),
                            UserId: userID,
                            RoomId: roomID
                        });

                    }
                    

                } catch(e){
                    console.log(e);
                }
            }

            else if (data.endPath != true) {
                if (!notes[data.userID]) {
                    notes[data.userID] = [];
                }

                delete data['userID'];
                delete data['roomID'];

                notes[userID].push(data); 

            }
            else {

                try {

                    if (note) {

                        if (note.paths)
                            path = JSON.parse(note.paths)

                    }

                    path.push(notes[userID]);


                    if (note) {

                        await sql.query('UPDATE `team17db`.Notes SET paths=:pt WHERE UserId=:uid AND RoomId=:rid',
                            { replacements: { pt: JSON.stringify(path), uid: userID, rid: roomID }, type: Sequelize.QueryTypes.UPDATE })

                    } else {

                        var newNote = await Note.create({
                            text: null,
                            paths: JSON.stringify(path),
                            createdAt: new Date(),
                            updatedAt: new Date(),
                            UserId: userID,
                            RoomId: roomID
                        });

                    }
                    notes[userID] = [];
                   



                } catch (e) {
                    console.log(e);

                }

            }

         
        });

    });





}