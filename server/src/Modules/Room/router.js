import express from 'express';
import verifyLogin from '../../Middlewares/auth'
const router  = express.Router();
import {
    createRoom,
    joinRoom,
    getCanvas,
    createMessage,
    getchat,
    getUserNotes,
    getRoomHistory
    } from './controller';

router.post("/create",verifyLogin,createRoom);
router.get("/:id",verifyLogin,joinRoom);
router.get("/:id/getcanvas",verifyLogin,getCanvas);
router.post("/:id/notes",verifyLogin,getUserNotes);
router.get("/:id/history",verifyLogin,getRoomHistory);

//chaat
router.post("/message/:id",verifyLogin,createMessage);
router.get("/message/:id",verifyLogin,getchat);

export default router ;
