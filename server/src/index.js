import express from 'express';
import bodyParser from 'body-parser';
//transform the request to json array
import passport from 'passport';
 // passport used for authentication
import morgan from 'morgan';
import Sequelize from 'sequelize';

import sequelize from "./config.js";
import router from './mainRouter';
require("./Model/relations.js");
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);

import socketHandeler from './Modules/Room/socketHandle' ;

socketHandeler(io);



app.use(express.static(__dirname + '/public'));


/*
  //  MIDDLEWARES
*/
//express needs these 3 , these are middlewares , any request will pass by these 3 first

  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(passport.initialize());


 // app.use(morgan('combined'));

  app.use(function(req, res, next) { //allow cross origin requests
        res.setHeader("Access-Control-Allow-Methods", "POST, PUT, OPTIONS, DELETE, GET");
        res.header("Access-Control-Allow-Origin", req.headers.origin);
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,Authorization");
        res.header("Access-Control-Allow-Credentials",true);
        next();
  })
  // to allow anybody to request on my server when it's not on the same machine
  // but it will allow for the following type of requests only
/*
  //  ROUTES
*/
app.use('/api', router);
// so any url concerning the backend will start with /api


/*
  // SERVER INIT
*/

  const PORT = process.env.PORT || 11627;

  http.listen(PORT, err => {
    if (err) {
      console.error(err);
    } else {
      console.log(`App listen to port: ${PORT}`);
    }
  });
