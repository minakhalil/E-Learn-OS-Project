import bcrypt from 'bcryptjs';
import { jwtSecret } from '../config'
import jwt from'jsonwebtoken';

const verifyToken = async (req, res, next) => {

    var token = req.headers['authorization'];

    if (!token)
        return res.status(401).send({ err: 'No token provided.' });
    try {
        /*
          the data inside a JWT is encoded and signed, not encrypted!
          Signing data allows the data receiver to verify the authenticity of the source of the data
          y3ni e7na hna bnt2aked mn en el jwtSecret el geh fl token hwa hwa nafs el hash bta3 el 3ndna
          msln, we b3d keda ntala3 mno l data . fa nedman eno mab3ot mn 7aga et3amlet m3 el SERVER abl
          keda we wa5da mno l token msh hya el m2alefah 3ashan mkanetsh hate3rf el secret
        */
        var decoded = await jwt.verify(token, jwtSecret);
        req.userId = decoded.id;
        next();
    } catch (e) {
        return res.status(401).send({ err: 'Expired token.' });
    }


}
export default verifyToken;
