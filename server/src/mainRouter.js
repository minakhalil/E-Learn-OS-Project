import express from 'express';
const router = express.Router();
import verifyLogin from './Middlewares/auth'
import { UserModule } from './Modules/User';
import { RoomModule } from './Modules/Room';
import {routerRoom} from './Modules/Room/router'


router.use('/user', UserModule);
router.use('/room',RoomModule);
//UserModule here isthe module name , it goes directly to the index.js , which export router 

export default router;
