import {sql} from '../config'
import Message from './MessageModel.js'
import Note from './NoteModel.js'
import Room from './RoomModel.js'
import User from './UserModel.js'
import UserRoom from './UserRooms.js'

import randomstring from 'randomstring'

import fs from 'fs';
import readline from 'readline';
import stream from 'stream'



async function forceTables(callback) {
  await sql.dropAllSchemas();

  await User.sync({force: true})
  await Room.sync({force: true})
  await Message.sync({force: true})
  await Note.sync({force: true})
  await UserRoom.sync({force: true})

  //called here as callback to make sure that the tables are dropped first then the seeds are feeded to the DB
  await callback();
}

async function seeds(){
  // used these before to add data from input file
  //let instream = fs.createReadStream('./input');
  //let outstream = new stream();
  //let rl = readline.createInterface(instream, outstream);
  //rl.on('line', function (line) {DB.User.create({username:line, showname:line, password:'12345',createdAt: new Date(), updatedAt: new Date()});});
  //rl.on('close', function (line) {console.log('     * done adding users.');});

  for (let u = 0; u < 15; u++) {//password is 12345
    let ttype = 0;
    if (!(u % 3)) ttype=1;
    await User.create({username:"user"+(1+u),showname:"user"+(1+u),password:'$2y$10$It78IHcJ6IvRa.c415D8SOM2.AgRUAn2a3ab240OScxQuu/x955QC',type:ttype,createdAt: new Date(), updatedAt: new Date()})
    let r = randomstring.generate({ length: 18, charset: 'alphabetic'});
    if (ttype) await Room.create({name:"room"+(1+u), text:"no text", paths: r, createdAt: new Date(), updatedAt: new Date(), OwnerId: u+1})
  }

  for (let i = 1; i < 6; i++){
    let arr = [];
    for (let j = 0 ; j < 5; j++){
      let randomUser = Math.floor(Math.random() * 15) + 1;
      if (!arr.includes(randomUser))
        sql.query("INSERT INTO `UserRooms` (`createdAt`, `updatedAt`, `room_id`, `user_id`) VALUES ('" + new Date().toMysqlFormat() + "', '" + new Date().toMysqlFormat() + "' , " + i + ", " + randomUser + ");");
      arr.push(randomUser);
    }
  }

  // :O el error kan eni 7atet 'dool' mesh `dool`
}

Note.belongsTo(User);
Note.belongsTo(Room);

Message.belongsTo(User);
Message.belongsTo(Room);

Room.belongsTo(User, {foreignKey: 'OwnerId'});

Room.belongsToMany(User, { as: 'Users', through: { model: UserRoom, unique: false }, foreignKey: 'room_id' });
User.belongsToMany(Room, { as: 'Rooms', through: { model: UserRoom, unique: false }, foreignKey: 'user_id' });

// npm_config_create is set using environment variables file from terminal flag --create=true
if (process.env.npm_config_create){
  console.log("* creating tables");
  forceTables(seeds);
  console.log("* adding data to tables");

}

function twoDigits(d) {
    if(0 <= d && d < 10) return "0" + d.toString();
    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
    return d.toString();
}
Date.prototype.toMysqlFormat = function() {
    return this.getUTCFullYear() + "-" + twoDigits(1 + this.getUTCMonth()) + "-" + twoDigits(this.getUTCDate()) + " " + twoDigits(this.getUTCHours()) + ":" + twoDigits(this.getUTCMinutes()) + ":" + twoDigits(this.getUTCSeconds());
};


export default User;
