import Sequelize from 'sequelize';
import {sql} from "../config.js";

//USER MODEL
// type 0 -> teacher
// type 1 -> student
const User = sql.define('User', {
  username: {
    type: Sequelize.STRING,
    unique:true,
    allowNull: false
  },
  showname: {
    type: Sequelize.STRING,
    allowNull: false
  },
  password: {
    type: Sequelize.STRING,
    allowNull: false
  },
  avatar: {
    type: Sequelize.STRING,
    defaultValue:'https://www.shareicon.net/data/512x512/2016/08/05/806962_user_512x512.png',
    allowNull: false
  },
  type: {
    type: Sequelize.INTEGER,
    defaultValue:1,
    allowNull: false
  },
});

User.prototype.toJSON =  function () {
  var values = Object.assign({}, this.get());

  delete values.password;
  return values;
}

export default User;
