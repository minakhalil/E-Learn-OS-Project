import Sequelize from 'sequelize';
import {sql} from "../config.js";

const Note = sql.define('Note', {
  text: {
    type: Sequelize.TEXT,
    allowNull: true
  },
  paths: {
    type: Sequelize.TEXT,
    allowNull: true
  }

});

export default Note;
