import Sequelize from 'sequelize';
import {sql} from "../config.js";

const Message = sql.define('Message', {
  message: {
    type: Sequelize.TEXT,
    allowNull: true
  }
});

export default Message;