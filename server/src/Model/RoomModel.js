import Sequelize from 'sequelize';
import {sql} from "../config.js";


const Room = sql.define('Room', {
  name: {
    type: Sequelize.STRING,
    unique:true,
    allowNull: false
  },
  text: {
    type: Sequelize.TEXT,
    allowNull: true
  },
  paths: {
    type: Sequelize.TEXT,
    allowNull: true
  }

});

export default Room;