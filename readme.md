> This project contains a front seed and a back seed, each one is runnable on it's own, and both of them should be served to run the project.

## BackSide:
    > Installation:
        - After cloning the project run
            $ sudo npm install
          inside the "server" folder, that would install all the project dependencies
        - create an empty schema called 'elearn-os' in MySQL
    > Running:
        Inside the "server" folder
        - to run the server run
            $ npm run start
            // the script in run is :  npm run watch & npm run run &
            // the & ... & means the two processes will run in parallel in the same terminal window (as we are watching using nodemon and Babel) - BUT you should close the process manualy 'kill -9 {psID}'
        - to run the server and create new database tables (besides dropping old ones and feeding the seeds)
           $ npm run start --create=true


## FrontSide:
    > Installation:
        - After cloning the project run
            $ sudo npm install
          inside the "client" folder, that would install all the project
          dependencies except the Angular-Cli which you will need to install
          by typing
            $ sudo npm install @angular/cli -g
    > Running:
        Inside the "client" folder
        - to build the front using angular run
            $ ng build
        - to serve the front side run
            $ ng serve

## General Todo:
- [x] create new project with packages
- [x] build project structure and modules
- [x] git repo
- [x] finish schema
- [x] create database models

## Front Todo:
- [x] Screen1: login
- [ ] Screen1-edits: login don't show errors when server is unreachable
- [x] Screen2: profile container
- [x] Screen3: Room container
- [x] component1: board component (for Room container)
- [ ] component2: userList (not clickable) with user notes (for Room container)
- [ ] component3: chatbox (for Room container)
- [ ] component4: sharelink button, savescreenshot button (for Room container)
- [ ] component5: starting a new room form (for Profile container)
- [x] component6: join a room, it's a button with a text box to paste the link into (for Profile container)
- [x] component7: profile data, not clickable, contains name and image
- [x] component8: old rooms joined, not clickable list of links with snapshots and links (for Profile)
- [x] component9: logout button (for Profile container)

## Back Todo:
- [x] userEndpoint1: login
- [x] userEndpoint2: get user data (for profile) [http://localhost:3000/api/user/me?userId=28]
- [x] userEndpoint3: get user old rooms
- [x] roomEndpoint1: create new room
- [ ] roomEndpoint3: get room current users (names, pics, notes and data ..)
- [ ] roomEndpoint4: join a room
- [ ] roomEndpoint5: get shareable link
