cd server
npm run build
cd ..
rm -r client/dist
rm -r server/dist/public
cd client
ng build --aot --prod
cd ..
mkdir server/dist/public
cp -R client/dist/. server/dist/public
tar -czvf out.tar.gz server/dist


